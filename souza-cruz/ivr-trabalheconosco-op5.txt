//--------------------------------
// 5 - Trabalhe Conosco
//  Akiva/Moov TI - 15/09/2019
//--------------------------------
context ivr-trabalheconosco-op5
{
	s => {		
        // Record client history            
        Noop(-------> Teste Sub Insert_Hist_Client_SC <-------);            
        Set(context=ivr-trabalheconosco-op5);
        Set(context_name=trabalhe_conosco);
        Set(NOSSAURA-AGI-PATH=/etc/asterisk/agi-bin/ivr-sc-nossaura);
        AGI(agi://${FastAGIServer}:${FastAGIPort}/Insert_Hist_Client_SC); 

		Agi(/etc/asterisk/agi-bin/ura-custom/getTimeCondition.php,TRABALHECONOSCO);
        Agi(${AGI_PATH}/gway/SetChannelVariable.pl);
		Set(__opcao_ura=trabalhe conosco);    
		Set(__optionstarttime=${STRFTIME(${EPOCH},,%s)}); 
		MSet(__entrada_dados="",__descricao_entrada="",__valor_entrada="",__retorno_dados="",__descricao_retorno="",__valor_retorno="");  

		Noop(Entrada IVR Consumidor ::: ${CALLERID(num)});
		//Set(context_name=1000);
		Agi(${AGI_PATH}/ura/getUra.php,1000,${SERVERID}); //Substituir o 1000 de acordo com a necessidade do sistema		

		Answer();
		Set(CHANNEL(language)=pt_BR);
		Set(TIMEOUT(response)=8);
		Set(TIMEOUT(digit)=5);
		Set(COUNT=1);
		instrucoes:
		Noop( ------------- Instrucoes ------------- );
		Playback(${AudioPath}/os_dados_coletados_durante); // Os dados coletados durante a ligação poderão ser utilizados para comunicação, atendimento,  pesquisas de natureza anônima e aprimoramento dos processos da Souza Cruz.

		Noop(Validacao de Registro CPF::: ${CALLERID(num)});
		Set(LOOPCPF=0);		
		Digiteseucpf:
		Read(__SCSAV-CVAREJO-CPF-CNPJ,${AudioPath}/por_favor_digite_o_seu_cpf,11);
		CONSULTA:
		switch("${LEN(${SCSAV-CVAREJO-CPF-CNPJ})}")
		{
			case "11": // CPF
				Agi(${NOSSAURA-AGI-PATH}/validaDocumento.php,${SCSAV-CVAREJO-CPF-CNPJ});

				// Entrada usuario
				Set(__entrada_dados=sim);
				Set(__descricao_entrada=verifica cpf cadastrados com a souza cruz);
				Set(__valor_entrada=${SCSAV-CVAREJO-CPF-CNPJ});				
				// Retorno integracao
				Set(__retorno_dados=sim);
				Set(__descricao_retorno=Y para documento valido);
				Set(__valor_retorno=${DOCUMENTO-VALIDO});	

                Noop(-----> DOCUMENTO-VALIDO: ${DOCUMENTO-VALIDO} <-----);
				if ("${DOCUMENTO-VALIDO}" = "Y")
				{
					Set(__opcao_ura_num=${opcao_ura_num}.d1);
					AGI(agi://${FastAGIServer}:${FastAGIPort}/Insert_ura_history);
					Set(EventID=120200);
					Set(Destination=${ID_IVR}|${EXTEN});
					Agi(${AGI_PATH}/gway/SendEventLog.pl);
					Set(EventID=120703);
					Set(Source=${CALLERID(num)});
					Set(Destination=${ID_IVR}|${TIPO}|${O${EXTEN}_DS}|${ID_Recording});
					Agi(${AGI_PATH}/gway/SendEventLog.pl);
					Playback(${AudioPath}/este_servico_e_exclusivo_para_maiores_de_18_anos_e_as_ligacoes_poderao_ser_gravadas_para_uso_exclusivo_da_empresa);
                    //goto ivr-consumidor-op2-validado,2,start;
                    goto ivr-consumidor-op2-d1,2,start;
					Busy (3);
					Hangup();
				} 
				else 
				{
					Set(__opcao_ura_num=${opcao_ura_num}.d0);
					AGI(agi://${FastAGIServer}:${FastAGIPort}/Insert_ura_history);										
					if(${LOOPCPF} > 1)
					{
						Playback(${AudioPath}/este_servico_e_exclusivo_para_maiores_de_18_anos_e_as_ligacoes_poderao_ser_gravadas_para_uso_exclusivo_da_empresa);
						Playback(${AudioPath}/por_favor_ligue_novamente_com_as_informacoes_solicitadas_para_que_possamos_atende-lo_a_souza_cruz_agradece_a_sua_ligacao_e_lhe_deseja);						
						goto finaliza_trabalheconosco,s,1;
						Hangup();	
					}														
					else 
					{								
						Read(SCSAV-CVAREJO-CPF-CNPJ,${AudioPath}/dados_incorretos_tecle_pausadamente_o_numero_do_seu_cpf,11); // Audio : "Dados incorretos."	
						Set(LOOPCPF=${MATH(${LOOPCPF}+1,int)});
						goto s,CONSULTA;								
					};	
				};
			break;
			default:
					if(${LOOPCPF} > 1)
					{
                        Set(__opcao_ura_num=${opcao_ura_num}.d0);
                        AGI(agi://${FastAGIServer}:${FastAGIPort}/Insert_ura_history);                        
						Playback(${AudioPath}/este_servico_e_exclusivo_para_maiores_de_18_anos_e_as_ligacoes_poderao_ser_gravadas_para_uso_exclusivo_da_empresa);
						Playback(${AudioPath}/por_favor_ligue_novamente_com_as_informacoes_solicitadas_para_que_possamos_atende-lo_a_souza_cruz_agradece_a_sua_ligacao_e_lhe_deseja);						
						goto finaliza_trabalheconosco,s,1;
						Hangup();	
					}														
					else 
					{								
						Read(SCSAV-CVAREJO-CPF-CNPJ,${AudioPath}/dados_incorretos_tecle_pausadamente_o_numero_do_seu_cpf,11); // Audio : "Dados incorretos."	
						Set(LOOPCPF=${MATH(${LOOPCPF}+1,int)});
						goto s,CONSULTA;								
					};
			break;
		}
	}

    h => {
        if("${ligacao_desconectada}" != "interno")
        {
            Set(__ligacao_desconectada=externo);            
        }
        if("${status_da_opcao}" != "encaminhado para fila")
        {
            Set(__status_da_opcao=abandonado antes de entrar na fila);
        }        
        AGI(agi://${FastAGIServer}:${FastAGIPort}/Insert_ura_history);
    }	
};

context finaliza_trabalheconosco
{
	s => {
		Set(__ligacao_desconectada=interno);
		MSet(__entrada_dados="",__descricao_entrada="",__valor_entrada="",__retorno_dados="",__descricao_retorno="",__valor_retorno="");
        if("${status_da_opcao}" != "encaminhado para fila")
        {
            Set(__status_da_opcao=abandonado antes de entrar na fila);
        }		
		AGI(agi://${FastAGIServer}:${FastAGIPort}/Insert_ura_history);
		
        Wait(0.9); 
		// Regra Bom dia - Boa tarde - Boa Noite
		ifTime(00:00-11:59|*|*|*)
		{
			Playback(${AudioPath}/bom_dia);
		};
		ifTime(12:00-18:00|*|*|*)
		{
			Playback(${AudioPath}/boa_tarde);
		};
		ifTime(18:01-23:59|*|*|*)
		{
			Playback(${AudioPath}/boa_noite);
		};
		Hangup();
	}
};